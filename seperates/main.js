/*
Main.JS
For the main rant viewing screen on devrant.com.
*/

//i've put all this inside a scope because we don't want to mess up any global variables in the bundled JS
{
	function pageScript(){
		window.location = 'about:blank'; //just testing that everything works :)
	}

	var page="feed"
	var location = window.location.pathname.split('/')[0]
	if (location === page) {
		pageScript();
	}
}